import math



def EuclideanDistance(x1,y1,x2,y2):
    z1 = abs(x1-x2) ** 2
    z2 = abs(y1-y2) ** 2

    result  =  math.sqrt(z1 + z2)
    return result


def findPosition(data, char):
    listaPos = []
    for x in range(0, 8, 1):
        for y in range(0, 8, 1):
            if data[x][y] == char:
                nod = Pos()
                #print(' [' + str(x) + '][' + str(y) + '] ')
                nod.x = x
                nod.y = y
                listaPos.append(nod)
    return listaPos



def FuncHeuristic(listaObj, x , y):

    final = 0

    for n in listaObj :
        final += EuclideanDistance(x,y,n.x, n.y)

    final = final / len(listaObj)

    return final


class Pos :

    def __init__(self):
        self.x = 0
        self.y = 0


class Node:

    def __init__(self , fx ,fy , tx, ty ):
        self.data = [['x', '-', 'x', '-', 'x', '-', 'x', '-'],  # Cuadro de Juego
                     ['-', 'x', '-', 'x', '-', 'x', '-', '-'],
                     ['-', '-', 'x', '-', 'x', '-', '-', '-'],
                     ['-', '-', '-', 'x', '-', '-', '-', '-'],
                     ['-', '-', '-', '-', 'o', '-', '-', '-'],
                     ['-', '-', '-', 'o', '-', 'o', '-', '-'],
                     ['-', '-', 'o', '-', 'o', '-', 'o', '-'],
                     ['-', 'o', '-', 'o', '-', 'o', '-', 'o']]
        self.Pos_Objetivo = findPosition(self.data, 'x')
        self.from_x = fx
        self.from_y = fy
        self.to_x = tx
        self.to_y = ty
        self.heuristica = FuncHeuristic(self.Pos_Objetivo,tx,ty)


    def setData(self, value):
        self.data = value




def Movimientos(EstadoActual):

    Posicion = findPosition(EstadoActual.data , 'o')

    NuevosMovimientos = []
    i =0


    for pos in Posicion:

        mov1 = Node(pos.x , pos.y , pos.x -1 , pos.y +1)
        mov2 = Node(pos.x , pos.y , pos.x -1 , pos.y -1)
        mov3 = Node(pos.x , pos.y , pos.x -2 , pos.y +2)
        mov4 = Node(pos.x , pos.y , pos.x -2 , pos.y -2)
        i += 1

        if mov1.from_x - mov1.to_x == 1 and mov1.from_y < 7 and mov1.data[mov1.to_x][mov1.to_y] == '-':
            print(str(i) + ' mov1  from [' + str(mov1.from_x) + '][' + str(mov1.from_y) + '] to ['+ str(mov1.to_x) + '][' + str(mov1.to_y) + ']'  +  ' Heuristica : ' + str(mov1.heuristica))
            NuevosMovimientos.append(mov1)
        if mov2.from_x - mov2.to_x == 1 and mov2.from_y > 0 and mov2.data[mov2.to_x][mov2.to_y] == '-':
            print(str(i) + ' mov2  from [' + str(mov2.from_x) + '][' + str(mov2.from_y) + '] to [' + str(mov2.to_x) + '][' + str(mov2.to_y) + ']' +  ' Heuristica : ' + str(mov2.heuristica))
            NuevosMovimientos.append(mov2)
        if mov3.from_x - mov3.to_x == 2 and mov3.from_y < 6 and mov3.data[mov3.to_x][mov3.to_y] == '-' and mov3.data[mov3.to_x - 1][mov3.to_y - 1] != '-':
            print(str(i) + ' mov3  from [' + str(mov3.from_x) + '][' + str(mov3.from_y) + '] to [' + str(mov3.to_x) + '][' + str(mov3.to_y) + ']' +  ' Heuristica : ' + str(mov3.heuristica))
            NuevosMovimientos.append(mov3)
        if mov4.from_x - mov4.to_x == 2 and mov4.from_y > 1 and mov4.data[mov4.to_x][mov4.to_y] == '-' and mov4.data[mov3.to_x - 1][mov4.to_y - 1] != '-':
            print(str(i) + ' mov4  from [' + str(mov4.from_x) + '][' + str(mov4.from_y) + '] to [' + str(mov4.to_x) + '][' + str(mov4.to_y) + ']' +  ' Heuristica : ' + str(mov4.heuristica))
            NuevosMovimientos.append(mov4)

    return NuevosMovimientos



if __name__ == '__main__':

        DAT = Node(None , None, 0, 0)
        LISTM = Movimientos(DAT)
        print(len(LISTM))
        i = 10000
        for n in LISTM :
            if n.heuristica < i :
                i = n.heuristica

        indiceMin = LISTM.index(i)
        print(str(i) + 'Este es el Puto nodo que tienes que elegir from [' + str(LISTM[indiceMin].from_x) + '][' + str(LISTM[indiceMin].from_y) + '] to [' + str(LISTM[indiceMin].to_x) + '][' + str(LISTM[indiceMin].to_y) + ']' + ' Heuristica : ' + str(LISTM[indiceMin].heuristica))
































