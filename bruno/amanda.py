import random
from ai import ultimo 

# Amanda acepta un tablero del exterior, y lo modifica.
class Amanda():
    def __init__(self, player, board):
        self.player = player
        self.must_reorient = self.orientation(board)

    # Acepta el tablero del exterior
    #___

    def orientation(self, board):
        if self.player in board[0]:
            return False
        else:
            return True

    def reorient(self, board):
        new_board = []
        for row in board:
            new_row = []
            for col in list(reversed(row)):
                new_row.append(col)
            new_board.append(new_row)

        return list(reversed(new_board))

    def set_board(self, board):
        if self.must_reorient:
            self.board = self.reorient(board)
        else:
            self.board = board

    # ordena el tablereo para el exterior
    #___

    def disorient(self):
        return self.reorient(self.board)

    def spit_board(self):
        if self.must_reorient:
            return self.disorient()
        else:
            return self.board
    
    # Que piezas puedo mover?
    #___

    def find_my_pieces(self):
        piece_place = []
        for y in range(len(self.board)):
            for x in range(len(self.board[0])):
                if self.board[y][x] == self.player:
                    piece_place.append((y, x))
        return piece_place

    def emptyP(self, from_place):
        y, x = from_place
        if self.board[y][x] == '-':
            return True
        else:
            return False

    def in_board(self, from_place):
        y, x = from_place

        if y < 0 or y > len(self.board) - 1:
            return False

        if x < 0 or x > len(self.board[0]) - 1:
            return False

        return True

    def a_step(self, from_place):
        """Given one Quien, te dice todos los Dondes que puede
        llegar en un paso.
        """
        can_go_list = []
        y, x = from_place
        go_left = y+1, x-1
        go_right = y+1, x+1

        if self.in_board(go_left) and self.emptyP(go_left) :
            can_go_list.append(go_left)
        if self.in_board(go_right) and self.emptyP(go_right) :
            can_go_list.append(go_right)

        if not can_go_list:
            return False

        will_go_list = []

        for going in can_go_list:
            will_go_list.append([from_place, going])

        return will_go_list
    
    def all_moves(self):
        all_moves = []
        for from_place in self.find_my_pieces():
            if self.a_step(from_place):
                all_moves.extend(self.a_step(from_place))

        return all_moves

    # Muevelo!
    #___

    def muevelo(self, from_to):
        from_place, to_place = from_to
        y_from, x_from = from_place
        y_to, x_to = to_place

        self.board[y_to][x_to] = self.board[y_from][x_from]
        self.board[y_from][x_from] = '-'

    # movimiento aleatorio
    #___

    def rand_move(self):
        return random.choice(self.all_moves())

    # movimiento kevin
    #___

    def kev_step(self):
        self.muevelo(ultimo(self.board))
        

    # Mueveme
    #___

    def rand_step(self):
        self.muevelo(self.rand_move())

    # Muestra el tabero

    def print_board(self):
        for row in self.spit_board():
            print(row)
        print()
