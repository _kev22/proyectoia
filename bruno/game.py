from amanda import *

class Board():
    def __init__(self):
        self.board = [['x', '-', 'x', '-', 'x', '-', 'x', '-'],
                      ['-', 'x', '-', 'x', '-', 'x', '-', '-'],
                      ['-', '-', 'x', '-', 'x', '-', '-', '-'],
                      ['-', '-', '-', 'x', '-', '-', '-', '-'],
                      ['-', '-', '-', '-', 'o', '-', '-', '-'],
                      ['-', '-', '-', 'o', '-', 'o', '-', '-'],
                      ['-', '-', 'o', '-', 'o', '-', 'o', '-'],
                      ['-', 'o', '-', 'o', '-', 'o', '-', 'o']]

    def setBoard(self, board):
        self.board = board

    def giveBoard(self):
        return self.board

    def moveBlind(self, quien, donde):
        yQuien, xQuien = quien
        yDonde, xDonde = donde
        self.board[yDonde][xDonde] = self.board[yQuien][xQuien]
        self.board[yQuien][xQuien] = '-'

    def emptyP(self, donde):
        y, x = donde
        if self.board[y][x] == '-':
            return True
        else:
            return False

    def fullP(self, quien):
        return not(self.emptyP(quien))

    def inBoard(self, quien):
        y, x = quien
        if y < 0 or y > len(self.board) - 1:
            return False
        if x < 0 or x > len(self.board[0]) - 1:
            return False
        return True

    def canMoveFromP(self, quien):
        return self.inBoard(quien) and self.fullP(quien)

    def canMoveToP(self, donde):
        return self.inBoard(donde) and self.emptyP(donde)

    def moveSee(self, quien, donde):
        if self.canMoveFromP(quien) and self.canMoveToP(donde):
            self.moveBlind(quien, donde)
            return True
        else:
            return False
    
    def stepDownP(self, quien, donde):
        y, x = quien
        yD, xD = donde
        if yD == y+1 and (xD == x - 1 or xD == x + 1):
            return True
        return False
    
    def stepUpP(self, quien, donde):
        y, x = quien
        yD, xD = donde
        if yD == y-1 and (xD == x - 1 or xD == x + 1):
            return True
        return False

    def moveSmart(self, quien, donde):
        if self.stepUpP(quien, donde):# or self.stepUpP(quien, donde):
            self.moveSee(quien, donde)
            return True
        else:
            return False

    # pretty printing
    #___

    def bPrint(self):
        for row in self.board:
            print(row)
        print()

    def bPretty(self):
        xAxis = "\t"
        for x in range(len(self.board[0])):
            xAxis += str(x) + "\t"
        print(xAxis)
            
        for y in range(len(self.board)):
            xAxis = "" + str(y)
            for x in range(len(self.board[0])):
                xAxis += "\t" + str(self.board[y][x])
            print(xAxis)

# brd = Board()
# brd.bPrint()
# brd.moveSmart([1, 1], [2, 0])
# brd.bPrint()
 

# el usuario siempre sera la x
class Game():
    def __init__(self):
        self.board = Board()
        self.players = ['x', 'o']
        self.ama = Amanda('x', self.board.giveBoard())
        self.ama.set_board(self.board.giveBoard())

#    def userInput(self):
#
#
#    # setup
#    def main():
#
    # run
    def go(self):
        while True:

            self.board.bPretty()
            print("que ficha quiere mover")
            yPlayer1Quien = int(input("posicion en y\n"))
            xPlayer1Quien = int(input("posicion en x\n"))

            print("donde quiere moverla?")
            yPlayer1Donde = int(input("posicion en y\n"))
            xPlayer1Donde = int(input("posicion en x\n"))

            player1Quien = yPlayer1Quien, xPlayer1Quien
            player1Donde = yPlayer1Donde, xPlayer1Donde

            if not(self.board.moveSmart(player1Quien, player1Donde)):
                print("esa movida no es valida")
                continue

            self.ama.set_board(self.board.giveBoard())
            self.ama.kev_step()
            self.board.setBoard(self.ama.spit_board())

bru = Game()
bru.go()
